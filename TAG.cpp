#include "TAG.h"

TAG::TAG()
{
	this->m_pictureId;
	this->m_userId;
}

void TAG::setPictureId(int pictureId)
{
	this->m_pictureId = pictureId;
}

int TAG::getPictureId()
{
	return this->m_pictureId;
}

void TAG::setUserId(int userId)
{
	this->m_userId = userId;
}

int TAG::getUserId()
{
	return this->m_userId;
}
