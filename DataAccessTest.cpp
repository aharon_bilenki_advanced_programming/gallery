#include "DataAccessTest.h"

DB::DB()
{
}

int DB::open_and_test_db()
{
	char* craeteTableUsers = "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL); ";
	char* createTableAlbums = "CREATE TABLE ALBUMS (ID INTEGER PRIMARY KEY, NAME TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, USER_ID INTEGER, FOREIGN KEY (USER_ID) REFERENCES USERS(ID)); ";
	char* createTablePictures = "CREATE TABLE PICTURES (ID INTEGER PRIMARY KEY, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE ETXT NOT NULL, ALBUM_ID INTEGER, FOREIGN KEY (ALBUM_ID) REFERENCES ALBUMS(ID)); ";
	char* createTableTags = "CREATE TABLE TAGS (ID INTEGER PRIMARY KEY, PICTURE_ID INTEGER, FOREIGN KEY (PICTURE_ID) REFERENCES PICTURES(ID), USER_ID INTEGER, FOREIGN KEY (USER_ID) REFERENCES USERS(ID)); ";
	char* insertUsers = "INSERT INTO USERS (ID, NAME) values (1, 'aharon1'), (2, 'aharon2'), (3, 'aharon3'); ";
	char* addAlbums = "INSERT INTO ALBUMS (ID, NAME, CREATION_DATE, USER_ID) values (1, 'Album1', '12/4/2018', 1), (2, 'Album2', '12/4/2018', 2), (3, 'Album3', '12/4/2018', 3); ";
	char* addPictures = "INSERT INTO PICTURES (ID, NAME, LOCATION, CREATION_DATE, ALBUM_ID) values (1, 'Picture1', 'p3.png', '12/4/2018', 1), (2, 'Picture2', 'p2.png', '12/4/2018', 1), (3, 'Picture3', 'p1.png', '12/4/2018', 2), (4, 'Picture4', 'Downloads/Picture4.jpg', '12/4/2018', 2), (5, 'Picture5', 'p5.png', '12/4/2018', 3), (6, 'Picture6', 'p9.png', '12/4/2018', 3); ";
	char* addTags = "INSERT INTO TAGS (ID, PICTURE_ID, USER_ID) values (1, 1, 1), (2, 1, 2), (3, 2, 1), (4, 2, 3), (5, 3, 2), (6, 3, 1), (7, 4, 2), (8, 4, 3), (9, 5, 3), (10, 5, 1), (11, 6, 3), (12, 6, 2); ";
	char* addWrongPic = "INSERT INTO PICTURES (ID, NAME, LOCATION, CREATION_DATE, ALBUM_ID) values (7, 'My femily', 'Downloads/MyFamily', '13/4/2018', 3); ";
	char* updatePicName = "UPDATE PICTURES set NAME='My Family' WHERE NAME='My femily'; ";
	char* deleteUser = "DELETE FROM TAGS WHERE USER_ID=3; DELETE FROM PICTURES WHERE USER_ID=3; DELETE FROM ALBUMS WHERE USER_ID=3; DELETE FROM USERS WHERE ID=3; ";
	char **errMessage = nullptr;
	sqlite3* db;
	string dbFileName = "GalleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		cout << "Failed to open DB" << endl;
		return -1;
	}
	res = sqlite3_exec(db, craeteTableUsers, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, createTableAlbums, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, createTablePictures, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, createTableTags, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, insertUsers, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, addAlbums, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, addPictures, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, addTags, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, addWrongPic, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, updatePicName, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	res = sqlite3_exec(db, deleteUser, nullptr, nullptr, errMessage);
	if (res != SQLITE_OK)
		return false;
	sqlite3_close(db);
	db = nullptr;
	return 1;
}
