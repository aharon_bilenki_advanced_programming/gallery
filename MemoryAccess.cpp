﻿#include <map>
#include <algorithm>
#include <string>
#include "ItemNotFoundException.h"
#include "MemoryAccess.h"

sqlite3* db;

const std::string MemoryAccess::currentDateTime()
{
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	return buf;
}

albums albumList;
int callbackAlbumsList(void *data, int argc, char **argv, char **azColName)
{
	Album *albums = new Album();
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			albums->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			albums->setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			albums->setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID") {
			albums->setOwner(atoi(argv[i]));
		}
	}
	albumList.push_back(*albums);
	return 0;
}

void printAlbums1()
{
	albums_iter iter = albumList.begin();
	while (iter != albumList.end())
	{
		std::cout << std::setw(5) << "* " << iter->getCreationDate() << "  " << iter->getName() << std::endl;
		++iter;
	}
}

void MemoryAccess::printAlbums() 
{
	char **errMessage = nullptr;
	char* select = "SELECT * FROM ALBUMS; ";
	albumList.clear();
	int res = sqlite3_exec(db, select, callbackAlbumsList, nullptr, errMessage);

	if(albumList.empty()) {
		throw MyException("There are no existing albums.");
	}
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	printAlbums1();
}

//create db and biuld it
bool MemoryAccess::open()
{
	int p = 0;
	char **errMessage = nullptr;
	std::string dbFileName = "GalleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);
	if (res != SQLITE_OK) {
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return -1;
	}
	if (doesFileExist)
	{
		//create tables
		char* craeteTableUsers = "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL); ";
		char* createTableAlbums = "CREATE TABLE ALBUMS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, USER_ID INTEGER, FOREIGN KEY (USER_ID) REFERENCES USERS(ID)); ";
		char* createTablePictures = "CREATE TABLE PICTURES (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE ETXT NOT NULL, ALBUM_ID INTEGER, FOREIGN KEY (ALBUM_ID) REFERENCES ALBUMS(ID)); ";
		char* createTableTags = "CREATE TABLE TAGS (ID INTEGER PRIMARY KEY, PICTURE_ID INTEGER, USER_ID INTEGER, FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID), FOREIGN KEY(USER_ID) REFERENCES USERS(ID)); ";
		res = sqlite3_exec(db, craeteTableUsers, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;
		res = sqlite3_exec(db, createTableAlbums, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;
		res = sqlite3_exec(db, createTablePictures, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;
		res = sqlite3_exec(db, createTableTags, nullptr, nullptr, errMessage);
		if (res != SQLITE_OK)
			return false;
	}
	return true;
}


//dlete all db information
void MemoryAccess::clear()
{
	char **errMessage = nullptr;
	char* deleteAll = "DELETE FROM USERS; DELETE FROM ALBUMS; DELETE FROM PICTURES; DELETE FROM TAGS; ";
	int res = sqlite3_exec(db, deleteAll, nullptr, nullptr, errMessage);
}
// close the db
void MemoryAccess::close()
{
	sqlite3_close(db);
	db = nullptr;
}

auto MemoryAccess::getAlbumIfExists(const std::string & albumName)
{
	auto result = std::find_if(std::begin(m_albums), std::end(m_albums), [&](auto& album) { return album.getName() == albumName; });

	if (result == std::end(m_albums)) {
		throw ItemNotFoundException("Album not exists: ", albumName);
	}
	return result;

}

Album MemoryAccess::createDummyAlbum(const User& user)
{
	std::stringstream name("Album_" +std::to_string(user.getId()));

	Album album(user.getId(),name.str());

	for (int i=1; i<3; ++i)	{
		std::stringstream picName("Picture_" + std::to_string(i));

		Picture pic(i++, picName.str());
		pic.setPath("C:\\Pictures\\" + picName.str() + ".bmp");

		album.addPicture(pic);
	}

	return album;
}

const std::list<Album> MemoryAccess::getAlbums() 
{
	return m_albums;
}

std::list<Album> albumsOfUser;
int callbackAlbumsListOfUser(void *data, int argc, char **argv, char **azColName)
{
	Album *albums = new Album();
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			albums->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			albums->setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			albums->setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID") {
			albums->setOwner(atoi(argv[i]));
		}
	}
	albumsOfUser.push_back(*albums);
	return 0;
}

const std::list<Album> MemoryAccess::getAlbumsOfUser(const User& user) 
{	
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM ALBUMS where USER_ID=" + std::to_string(user.getId()) + "; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	albumsOfUser.clear();
	int res = sqlite3_exec(db, select, callbackAlbumsListOfUser, nullptr, errMessage);
	return albumsOfUser;
}

void MemoryAccess::createAlbum(const Album& album)
{
	char **errMessage = nullptr;
	std::stringstream osinsert("INSERT INTO ALBUMS (NAME, CREATION_DATE, USER_ID) values ('" + album.getName() + "', '" + album.getCreationDate() + "', " + std::to_string(album.getOwnerId()) + "); ");
	std::string tmp = osinsert.str();
	const char* insert = tmp.c_str();
	int res = sqlite3_exec(db, insert, nullptr, nullptr, errMessage);
}

void MemoryAccess::deleteAlbum(const std::string& albumName, int userId)
{
	char **errMessage = nullptr;
	std::stringstream osdeleete("DELETE FROM ALBUMS where NAME='" + albumName + "' AND USER_ID=" + std::to_string(userId) + "; ");
	std::string tmp = osdeleete.str();
	const char* deleete = tmp.c_str();
	int res = sqlite3_exec(db, deleete, nullptr, nullptr, errMessage);

}

bool MemoryAccess::doesAlbumExists(const std::string& albumName, int userId) 
{
	char **errMessage = nullptr;
	std::stringstream oscheck("SELECT * FROM ALBUMS WHERE NAME='" + albumName + "' AND USER_ID=" + std::to_string(userId) + "; ");
	std::string tmp = oscheck.str();
	const char* check = tmp.c_str();
	int res = sqlite3_exec(db, check, callbackAlbumsList, nullptr, errMessage);
	if (!albumList.empty())
		return true;
	return false;
}

Album MemoryAccess::openAlbum(const std::string& albumName) 
{
	char **errMessage = nullptr;
	std::stringstream osinsert("SELECT * FROM ALBUMS where NAME='" + albumName + "'; ");
	std::string tmp = osinsert.str();
	const char* insert = tmp.c_str();
	int res = sqlite3_exec(db, insert, callbackAlbumsList, nullptr, errMessage);
	if (!albumList.empty())
		return albumList.front();

	throw MyException("No album with name " + albumName + " exists");
}

void MemoryAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture) 
{
	Album result = openAlbum(albumName);

	char **errMessage = nullptr;
	std::stringstream osinsert("INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) values ('" + picture.getName() + "', '" + picture.getPath() + "', '" + currentDateTime() + "', " + std::to_string(result.getId()) + "); ");
	std::string tmp = osinsert.str();
	const char* insert = tmp.c_str();
	int res = sqlite3_exec(db, insert, nullptr, nullptr, errMessage);


}

void MemoryAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) 
{
	Album result = openAlbum(albumName);

	char **errMessage = nullptr;
	std::stringstream osdeleete("DELETE FROM PICTURES where NAME='" + pictureName + "'; ");
	std::string tmp = osdeleete.str();
	const char* deleete = tmp.c_str();
	int res = sqlite3_exec(db, deleete, nullptr, nullptr, errMessage);


}

std::list<Picture> pictureList;
int callbackPicture(void *data, int argc, char **argv, char **azColName)
{
	Picture *picture = new Picture();
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			picture->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			picture->setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION") {
			picture->setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE") {
			picture->setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "ALBUM_ID") {
			picture->setAlbumId(argv[i]);
		}
	}
	pictureList.push_back(*picture);
	return 0;
}

void Printpic()
{
	pictures_iter iter = pictureList.begin();
	while (iter != pictureList.end())
	{
		std::cout << "   + Picture [" << iter->getId() << "] - " << iter->getName() <<
			"\tLocation: [" << iter->getPath() << "]\tCreation Date: [" <<
			iter->getCreationDate() << "]\tTags: [" << iter->getTagsCount() << "]" << std::endl;
		++iter;
	}
}

void listPicturesInAlbum1(int albumId)
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM PICTURES where ALBUM_ID=" + std::to_string(albumId) + "; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	pictureList.clear();
	int res = sqlite3_exec(db, select, callbackPicture, nullptr, errMessage);

	Printpic();
}

//check if the pic exists and return true  false
bool Dos_pic_exists(const std::string & pictureName, int albumId)
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM PICTURES where NAME='" + pictureName + "' AND ALBUM_ID=" + std::to_string(albumId) + "; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	pictureList.clear();
	int res = sqlite3_exec(db, select, callbackPicture, nullptr, errMessage);
	if (!pictureList.empty())
		return true;
	return false;
}

Picture Get_pic(const std::string & pictureName)
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM PICTURES where NAME='" + pictureName + "'; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	pictureList.clear();
	int res = sqlite3_exec(db, select, callbackPicture, nullptr, errMessage);
	return pictureList.front();
}

//tag user 
void MemoryAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM PICTURES where NAME='" + pictureName + "'; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	pictureList.clear();
	int res = sqlite3_exec(db, select, callbackPicture, nullptr, errMessage);
	Picture result = pictureList.front();

	std::stringstream osinsert("INSERT INTO TAGS (PICTURE_ID, USER_ID) values (" + std::to_string(result.getId()) + ", " + std::to_string(userId) + "); ");
	tmp = osinsert.str();
	const char* insert = tmp.c_str();
	res = sqlite3_exec(db, insert, nullptr, nullptr, errMessage);

/*	auto result = getAlbumIfExists(albumName);

	(*result).tagUserInPicture(userId, pictureName);*/
}

//DELET TAG FROM USER
void MemoryAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM PICTURES where NAME='" + pictureName + "'; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	int res = sqlite3_exec(db, select, callbackPicture, nullptr, errMessage);
	Picture result = pictureList.front();

	std::stringstream osdeleete("DELETE FROM TAGS where PICTURE_ID=" + std::to_string(result.getId()) + "and USER_ID=" + std::to_string(userId) + "; ");
	tmp = osdeleete.str();
	const char* deleete = tmp.c_str();
	res = sqlite3_exec(db, deleete, nullptr, nullptr, errMessage);


}

void MemoryAccess::closeAlbum(Album& pAlbum)
{
}

// ******************* User ******************* 
std::list<User> userList;
int callbackUsers(void *data, int argc, char **argv, char **azColName)
{
	User *user = new User();
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "ID") {
			user->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME") {
			user->setName(argv[i]);
		}
	}
	userList.push_back(*user);
	return 0;
}

void printUsers1()
{
	users_iter iter = userList.begin();
	while (iter != userList.end())
	{
		std::cout << iter->getId() << " - " << iter->getName() << std::endl;
		++iter;
	}
}

void MemoryAccess::printUsers()
{
	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;

	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM USERS;");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	userList.clear();
	int res = sqlite3_exec(db, select, callbackUsers, nullptr, errMessage);
	printUsers1();

/*	for (const auto& user: m_users) {
		std::cout << user << std::endl;
	}*/
}

User MemoryAccess::getUser(int userId) 
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM USERS where ID=" + std::to_string(userId) + "; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	int res = sqlite3_exec(db, select, callbackUsers, nullptr, errMessage);
	if (!userList.empty())
		return userList.front();

/*	for (const auto& user : m_users) {
		if (user.getId() == userId) {
			return user;
		}
	}*/

	throw ItemNotFoundException("User", userId);
}

void MemoryAccess::createUser(User& user)
{
	char **errMessage = nullptr;
	std::stringstream osinsert("INSERT INTO USERS (ID, NAME) values (" + std::to_string(user.getId()) + ", '" + user.getName() + "'); ");
	std::string tmp = osinsert.str();
	const char* insert = tmp.c_str();
	int res = sqlite3_exec(db, insert, nullptr, nullptr, errMessage);

	//m_users.push_back(user);
}

void MemoryAccess::deleteUser(const User& user)
{
	char **errMessage = nullptr;
	std::stringstream osdeleete("DELET FROM ALBUMS where USER_ID=" + std::to_string(user.getId()) + "; ");
	std::string tmp = osdeleete.str();
	const char* deleete = tmp.c_str();
	int res = sqlite3_exec(db, deleete, nullptr, nullptr, errMessage);

	std::stringstream osdeleete1("DELET FROM USERS where ID=" + std::to_string(user.getId()) + "; ");
	tmp = osdeleete1.str();
	deleete = tmp.c_str();
	res = sqlite3_exec(db, deleete, nullptr, nullptr, errMessage);

/*	if (doesUserExists(user.getId())) {
		for (const auto& album : m_albums) {
			if (album.getOwnerId() == user.getId()) {
				m_albums.remove(album);
				break;
			}
		}
		for (auto iter = m_users.begin(); iter != m_users.end(); ++iter) {
			if (*iter == user) {
				iter = m_users.erase(iter);
				return;
			}
		}
	}*/
}

bool MemoryAccess::doesUserExists(int userId) 
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM USERS where ID=" + std::to_string(userId) + "; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	int res = sqlite3_exec(db, select, callbackUsers, nullptr, errMessage);
	if (!userList.empty())
		return true;

/*	auto iter = m_users.begin();
	for (const auto& user : m_users) {
		if (user.getId() == userId) {
			return true;
		}
	}*/
	
	return false;
}

// user statistics
int MemoryAccess::countAlbumsOwnedOfUser(const User& user) 
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM ALBUMS where USER_ID=" + std::to_string(user.getId()) + "; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	int res = sqlite3_exec(db, select, callbackAlbumsList, nullptr, errMessage);
	return albumList.size();

/*	int albumsCount = 0;

	for (const auto& album: m_albums) {
		if (album.getOwnerId() == user.getId()) {
			++albumsCount;
		}
	}

	return albumsCount;*/
}

int MemoryAccess::countAlbumsTaggedOfUser(const User& user) 
{
	int albumsCount = 0;

	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM ALBUMS;");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	int res = sqlite3_exec(db, select, callbackAlbumsList, nullptr, errMessage);
	for (auto iter = albumList.begin(); iter != albumList.end(); ++iter)
	{
		std::stringstream osselect("SELECT * FROM PICTURES JOIN TAGS ON PICTURES.ID=TAGS.PICTURE_ID where TAGS.USER_ID=" + std::to_string(user.getId()) + "AND ALBUM_ID=" + std::to_string(iter->getId()) + "; ");
		std::string tmp = osselect.str();
		const char* select = tmp.c_str();
		res = sqlite3_exec(db, select, callbackPicture, nullptr, errMessage);
		if (!pictureList.empty())
			albumsCount++;
	}

/*	for (const auto& album: m_albums) {
		const std::list<Picture>& pics = album.getPictures();
		
		for (const auto& picture: pics)	{
			if (picture.isUserTagged(user))	{
				albumsCount++;
				break;
			}
		}
	}*/

	return albumsCount;
}

std::list<TAG> tagList;
int callbackTags(void *data, int argc, char **argv, char **azColName)
{
	tagList.clear();
	TAG *tag = new TAG();
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "PICTURE_ID") {
			tag->setPictureId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "USER_ID") {
			tag->setUserId(atoi(argv[i]));
		}
	}
	tagList.push_back(*tag);
	return 0;
}

int MemoryAccess::countTagsOfUser(const User& user) 
{
	char **errMessage = nullptr;
	std::stringstream osselect("SELECT * FROM TAGS where USER_ID=" + std::to_string(user.getId()) + "; ");
	std::string tmp = osselect.str();
	const char* select = tmp.c_str();
	int res = sqlite3_exec(db, select, callbackTags, nullptr, errMessage);
	return tagList.size();

/*	int tagsCount = 0;

	for (const auto& album: m_albums) {
		const std::list<Picture>& pics = album.getPictures();
		
		for (const auto& picture: pics)	{
			if (picture.isUserTagged(user))	{
				tagsCount++;
			}
		}
	}

	return tagsCount;*/
}

float MemoryAccess::averageTagsPerAlbumOfUser(const User& user) 
{
	int albumsTaggedCount = countAlbumsTaggedOfUser(user);

	if ( 0 == albumsTaggedCount ) {
		return 0;
	}

	return static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount;
}

User MemoryAccess::getTopTaggedUser()
{
	std::map<int, int> userTagsCountMap;

	auto albumsIter = m_albums.begin();
	for (const auto& album: m_albums) {
		for (const auto& picture: album.getPictures()) {
			
			const std::set<int>& userTags = picture.getUserTags();
			for (const auto& user: userTags ) {
				//As map creates default constructed values, 
				//users which we haven't yet encountered will start from 0
				userTagsCountMap[user]++;
			}
		}
	}

	if (userTagsCountMap.size() == 0) {
		throw MyException("There isn't any tagged user.");
	}

	int topTaggedUser = -1;
	int currentMax = -1;
	for (auto entry: userTagsCountMap) {
		if (entry.second < currentMax) {
			continue;
		}

		topTaggedUser = entry.first;
		currentMax = entry.second;
	}

	if ( -1 == topTaggedUser ) {
		throw MyException("Failed to find most tagged user");
	}

	return getUser(topTaggedUser);
}

Picture MemoryAccess::getTopTaggedPicture()
{
	int currentMax = -1;
	const Picture* mostTaggedPic = nullptr;
	for (const auto& album: m_albums) {
		for (const Picture& picture: album.getPictures()) {
			int tagsCount = picture.getTagsCount();
			if (tagsCount == 0) {
				continue;
			}

			if (tagsCount <= currentMax) {
				continue;
			}

			mostTaggedPic = &picture;
			currentMax = tagsCount;
		}
	}
	if ( nullptr == mostTaggedPic ) {
		throw MyException("There isn't any tagged picture.");
	}

	return *mostTaggedPic;	
}

std::list<Picture> MemoryAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> pictures;

	for (const auto& album: m_albums) {
		for (const auto& picture: album.getPictures()) {
			if (picture.isUserTagged(user)) {
				pictures.push_back(picture);
			}
		}
	}

	return pictures;
}
