#pragma once
#include <string>

class TAG
{
public:
	TAG();
	
	void setPictureId(int pictureId);
	int getPictureId();

	void setUserId(int userId);
	int getUserId();

private:
	int m_pictureId;
	int m_userId;
};